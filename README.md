# Splendid Adventures on a Sunday Afternoon

> A game for Ludum Dare 41

## Credits

* page flip sound by _flag2_ https://freesound.org/people/flag2/sounds/63318/
* background texture by Tim Jones https://www.flickr.com/photos/27613359@N03/14206231412/

## Build Setup

```bash
# install dependencies
$ npm install # Or yarn install

# build levels
$ npm run transform-levels

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).
