const cheerio = require("cheerio");
const fs = require("fs");
const path = require("path");

const levelsPath = path.resolve(__dirname, "..", "levels");
const outputPath = path.resolve(__dirname, "..", "components", "levels");
const fileNames = fs
  .readdirSync(levelsPath)
  .filter(fileName => fileName.endsWith(".svg"));
fileNames.forEach(fileName => {
  console.log(`Tranforming ${fileName} ...`);

  const fileContent = fs.readFileSync(path.resolve(levelsPath, fileName));
  const $ = cheerio.load(fileContent, {
    xmlMode: true
  });

  $("#player").wrap(
    $(
      `<player :translation="playerTranslation" :image-rectangle="playerImageRectangleSvg" />`
    )
  );

  fs.writeFileSync(
    path.resolve(outputPath, `${fileName}.vue`),
    `<template>
  ${$.html()
    .replace(/<\?xml.*\?>/, "")
    .replace(/&apos;/g, "'")}
</template>

<script>
  import physics_mixin from "~/components/physics-mixin.vue";
  import Player from "~/components/player.vue";

  export default {
    mixins: [physics_mixin],
    components: {
      Player
    }
  };
</script>
`
  );
});

fs.writeFileSync(
  path.resolve(outputPath, "levels.json"),
  JSON.stringify(
    fileNames.map(fileName => fileName.replace(/\.svg$/, "")),
    null,
    2
  )
);
