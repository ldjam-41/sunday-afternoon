import mutationTypes from "./mutation-types";

export default {
  [mutationTypes.SET_CURRENT_LEVEL](state, name) {
    state.currentLevelName = name;
  },
  [mutationTypes.TOGGLE_DEBUG](state) {
    state.debug = !state.debug;
  },
  [mutationTypes.SET_WALKING_DIRECTION](state, walkingDirection) {
    Object.assign(state, { walkingDirection });
  }
};
