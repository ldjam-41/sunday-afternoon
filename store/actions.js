import mutationTypes from "./mutation-types";

export default {
  setCurrentLevelName({ commit }, name) {
    commit(mutationTypes.SET_CURRENT_LEVEL, name);
  },
  setWalkingDirection({ commit }, walkingDirection) {
    commit(mutationTypes.SET_WALKING_DIRECTION, walkingDirection);
  },
  toggleDebug({ commit }) {
    commit(mutationTypes.TOGGLE_DEBUG);
  }
};
